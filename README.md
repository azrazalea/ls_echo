ls_echo
=======

Small script that will wait for a TCP connection and respond with the contents of the configured directory.

Usage: ./ls_echo.rb \<port\> \<directory\>
       ./ls_echo.py \<port\> \<directory\>
