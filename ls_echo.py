#!/usr/bin/python
# Copyright (c) 2013, Lily Carpenter
# All rights reserved.

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.

# * Neither the name of the Lily Carpenter nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

usage = "./ls_echo.py <port> <directory>"

import sys
import os
import time
import fcntl
import socket
import threading
import SocketServer

if len(sys.argv) != 3:
    print usage
    exit(1)

PORT= sys.argv[1]
DIRECTORY = sys.argv[2]

port = int(PORT)
directory = os.path.abspath(os.path.expanduser(DIRECTORY))
if not os.path.isdir(directory):
    raise TypeError("Path " + directory + " does not represent a valid directory.")

class ThreadedRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        response = "\n".join(os.listdir(directory))
        self.request.send(response + "\n")

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

if __name__ == "__main__":
    # No standard daemon module, thanks http://daemonize.sourceforge.net/daemonize.txt
    process_id = os.fork()
    if process_id < 0:
        sys.exit(1)
    elif process_id != 0:
        sys.exit(0)

    process_id = os.setsid()
    if process_id == -1:
        sys.exit(1)

    devnull = '/dev/null'
    if hasattr(os, "devnull"):
        devnull = os.devnull
    null_descriptor = open(devnull, 'rw')
    for descriptor in (sys.stdin, sys.stdout, sys.stderr):
        descriptor.close()
        descriptor = null_descriptor

    os.umask(027)

    os.chdir('/')

    lockfile = open('/tmp/lsecho.lock', 'w')
    fcntl.lockf(lockfile, fcntl.LOCK_EX|fcntl.LOCK_NB)

    lockfile.write('%s' %(os.getpid()))
    lockfile.flush()

    server = ThreadedTCPServer(("0.0.0.0", port), ThreadedRequestHandler)

    server.serve_forever()
