#!/usr/bin/ruby
# Copyright (c) 2013, Lily Carpenter
# All rights reserved.

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.

# * Neither the name of the Lily Carpenter nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

split_version = RUBY_VERSION.split(".")

if split_version[0] != "1" or split_version[1] != "9"
  raise(RuntimeError, "Ruby 1.9.x required, but ruby #{RUBY_VERSION} used")
end

usage = "./ls_echo.rb <port> <directory>"

if ARGV.length != 2
  puts(usage)
  exit 1
end

require 'socket'
require 'pathname'
PORT = ARGV[0]
DIRECTORY = ARGV[1]

# Check arguments
port = Integer(PORT)
directory = Pathname.new(DIRECTORY).expand_path

if not directory.directory?
    raise ArgumentError, "Path #{DIRECTORY} does not represent a valid directory."
end

server = TCPServer.new port
fork do
  Process.daemon
  loop do
    Thread.start(server.accept) do |client|
      dirs = directory.children.select { |c| c.directory? }\
                               .collect { |p| p.to_s }
      client.puts dirs.join("\n")
      client.close
    end
  end
end
